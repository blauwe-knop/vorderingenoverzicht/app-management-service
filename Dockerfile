FROM golang:1.23.1-alpine3.20 AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/app-management-service/api
COPY ./cmd /go/src/app-management-service/cmd
COPY ./internal /go/src/app-management-service/internal
COPY ./pkg /go/src/app-management-service/pkg
COPY ./go.mod /go/src/app-management-service/
COPY ./go.sum /go/src/app-management-service/
WORKDIR /go/src/app-management-service
RUN go mod download \
 && go build -o dist/bin/app-management-service ./cmd/app-management-service

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/app-management-service/dist/bin/app-management-service /usr/local/bin/app-management-service
COPY --from=build /go/src/app-management-service/api/openapi.json /api/openapi.json
COPY --from=build /go/src/app-management-service/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/app-management-service"]
