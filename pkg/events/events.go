package events

var (
	AMS_1 = NewEvent(
		"ams_1",
		"started listening",
		Low,
	)
	AMS_2 = NewEvent(
		"ams_2",
		"server closed",
		High,
	)
	AMS_3 = NewEvent(
		"ams_3",
		"error creating registration db",
		VeryHigh,
	)
	AMS_4 = NewEvent(
		"ams_4",
		"received request for openapi spec as JSON",
		Low,
	)
	AMS_5 = NewEvent(
		"ams_5",
		"sent response with openapi spec as JSON",
		Low,
	)
	AMS_6 = NewEvent(
		"ams_6",
		"received request for openapi spec as YAML",
		Low,
	)
	AMS_7 = NewEvent(
		"ams_7",
		"sent response with openapi spec as YAML",
		Low,
	)
	AMS_8 = NewEvent(
		"ams_8",
		"failed to read openapi.json file",
		High,
	)
	AMS_9 = NewEvent(
		"ams_9",
		"failed to read openapi.yaml file",
		High,
	)
	AMS_10 = NewEvent(
		"ams_10",
		"failed to write fileBytes to response",
		High,
	)
	AMS_11 = NewEvent(
		"ams_11",
		"received request for registration list",
		Low,
	)
	AMS_12 = NewEvent(
		"ams_12",
		"sent response with registration list",
		Low,
	)
	AMS_13 = NewEvent(
		"ams_13",
		"received request to create registration",
		Low,
	)
	AMS_14 = NewEvent(
		"ams_14",
		"sent response with created registration",
		Low,
	)
	AMS_15 = NewEvent(
		"ams_15",
		"received request to fetch registration",
		Low,
	)
	AMS_16 = NewEvent(
		"ams_16",
		"sent response with registration",
		Low,
	)
	AMS_17 = NewEvent(
		"ams_17",
		"received request to update registration",
		Low,
	)
	AMS_18 = NewEvent(
		"ams_18",
		"sent response with updated registration",
		Low,
	)
	AMS_19 = NewEvent(
		"ams_19",
		"received request to delete registration",
		Low,
	)
	AMS_20 = NewEvent(
		"ams_20",
		"sent response with deleted registration",
		Low,
	)
	AMS_21 = NewEvent(
		"ams_21",
		"failed to encode response payload",
		High,
	)
	AMS_22 = NewEvent(
		"ams_22",
		"failed to decode request payload",
		High,
	)
	AMS_23 = NewEvent(
		"ams_23",
		"failed to fetch registration",
		High,
	)
	AMS_24 = NewEvent(
		"ams_24",
		"registration not found",
		High,
	)
	AMS_25 = NewEvent(
		"ams_25",
		"failed to get registration setting",
		High,
	)
	AMS_26 = NewEvent(
		"ams_26",
		"failed to update registration",
		High,
	)
	AMS_27 = NewEvent(
		"ams_27",
		"failed to delete registration",
		High,
	)
)
