package model

type Registration struct {
	Token        string   `json:"token"`
	AppPublicKey string   `json:"appPublicKey"`
	Bsn          string   `json:"bsn"`
	ClientId     string   `json:"clientId"`
	CreatedAt    JSONTime `json:"createdAt"`
	ExpiresAt    JSONTime `json:"expiresAt"`
}
