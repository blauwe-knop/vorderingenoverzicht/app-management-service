// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
)

type RegistrationRepository interface {
	List() (*[]model.Registration, error)
	Create(Registration model.Registration) (*model.Registration, error)
	Get(id string) (*model.Registration, error)
	Update(id string, Registration model.Registration) (*model.Registration, error)
	Delete(id string) (*model.Registration, error)
	healthcheck.Checker
}
