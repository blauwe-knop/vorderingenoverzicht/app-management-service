// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
)

type RegistrationClient struct {
	baseURL string
	apiKey  string
}

var ErrRegistrationNotFound = errors.New("registration does not exist")

func NewRegistrationClient(baseURL string, apiKey string) *RegistrationClient {
	return &RegistrationClient{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *RegistrationClient) List() (*[]model.Registration, error) {
	url := fmt.Sprintf("%s/registrations", s.baseURL)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get registrations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get registration list: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving registration list: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	registrations := []model.Registration{}
	err = json.Unmarshal(body, &registrations)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &registrations, nil
}

func (s *RegistrationClient) Create(registration model.Registration) (*model.Registration, error) {
	url := fmt.Sprintf("%s/registrations", s.baseURL)

	requestBodyAsJson, err := json.Marshal(registration)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create post registration request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post registration: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while creating registration: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnRegistration := model.Registration{}
	err = json.Unmarshal(body, &returnRegistration)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnRegistration, nil
}

func (s *RegistrationClient) Get(id string) (*model.Registration, error) {
	if id == "" {
		return nil, fmt.Errorf("failed to get registration, invalid id")
	}

	url := fmt.Sprintf("%s/registrations/%s", s.baseURL, id)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get registration request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get registration: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrRegistrationNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving registration: %d, id: %s", resp.StatusCode, id)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	registration := model.Registration{}
	err = json.Unmarshal(body, &registration)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, id: %s: %v", id, err)
	}

	return &registration, nil
}

func (s *RegistrationClient) Update(id string, registration model.Registration) (*model.Registration, error) {
	url := fmt.Sprintf("%s/registrations/%s", s.baseURL, id)

	requestBodyAsJson, err := json.Marshal(registration)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create update registration request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to update registration: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while updating registration: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnRegistration := model.Registration{}
	err = json.Unmarshal(body, &returnRegistration)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnRegistration, nil
}

func (s *RegistrationClient) Delete(id string) (*model.Registration, error) {
	url := fmt.Sprintf("%s/registrations/%s", s.baseURL, id)

	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to delete registrations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to delete registration: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while deleting registration: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	registration := model.Registration{}
	err = json.Unmarshal(body, &registration)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &registration, nil
}
func (s *RegistrationClient) GetHealthCheck() healthcheck.Result {
	name := "app-management-service"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
