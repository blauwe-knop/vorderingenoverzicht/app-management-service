// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
)

func handlerList(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationRepository, _ := context.Value(registrationRepositoryKey).(repositories.RegistrationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMS_11
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	registrations := registrationRepository.List(context)

	responseWriter.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(responseWriter).Encode(registrations)
	if err != nil {
		event := events.AMS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMS_12
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerCreate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationRepository, _ := context.Value(registrationRepositoryKey).(repositories.RegistrationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMS_13
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var registration model.Registration
	err := json.NewDecoder(request.Body).Decode(&registration)
	if err != nil {
		event := events.AMS_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	createdRegistration, err := registrationRepository.Create(context, registration)
	if err != nil {
		event := events.AMS_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*createdRegistration)
	if err != nil {
		event := events.AMS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMS_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerGet(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationRepository, _ := context.Value(registrationRepositoryKey).(repositories.RegistrationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMS_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	registration, err := registrationRepository.Get(context, chi.URLParam(request, "token"))
	if errors.Is(err, repositories.ErrRegistrationNotFound) {
		event := events.AMS_24
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}
	if err != nil {
		event := events.AMS_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*registration)
	if err != nil {
		event := events.AMS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMS_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerUpdate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationRepository, _ := context.Value(registrationRepositoryKey).(repositories.RegistrationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMS_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var registration model.Registration
	err := json.NewDecoder(request.Body).Decode(&registration)
	if err != nil {
		event := events.AMS_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	token := chi.URLParam(request, "token")

	_, err = registrationRepository.Get(context, token)
	if err != nil {
		event := events.AMS_25
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	_, err = registrationRepository.Update(context, registration, token)
	if err != nil {
		event := events.AMS_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(registration)
	if err != nil {
		event := events.AMS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMS_18
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerDelete(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationRepository, _ := context.Value(registrationRepositoryKey).(repositories.RegistrationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMS_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	token := chi.URLParam(request, "token")

	registration, err := registrationRepository.Get(context, token)
	if err != nil {
		event := events.AMS_25
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = registrationRepository.Delete(context, token)
	if err != nil {
		event := events.AMS_27
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*registration)
	if err != nil {
		event := events.AMS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMS_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
