// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
)

type RegistrationRepository interface {
	Create(ctx context.Context, newRegistration model.Registration) (*model.Registration, error)
	List(ctx context.Context) []model.Registration
	Get(ctx context.Context, token string) (*model.Registration, error)
	Update(ctx context.Context, newRegistration model.Registration, token string) (*model.Registration, error)
	Delete(ctx context.Context, token string) error
	healthcheck.Checker
}
