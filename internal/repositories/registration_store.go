// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
)

type RegistrationStore struct {
	log *zap.Logger

	registrationList []model.Registration
}

var ErrRegistrationNotFound = errors.New("registration does not exist")

func NewRegistrationStore(ctx context.Context, log *zap.Logger) *RegistrationStore {
	return &RegistrationStore{log: log, registrationList: []model.Registration{}}
}

func (s *RegistrationStore) Create(ctx context.Context, newRegistration model.Registration) (*model.Registration, error) {
	s.log.Debug("Create", zap.String("registration.Token", newRegistration.Token))

	s.registrationList = append(s.registrationList, newRegistration)

	return &newRegistration, nil
}

func (s *RegistrationStore) List(ctx context.Context) []model.Registration {
	return s.registrationList
}

func (s *RegistrationStore) Get(ctx context.Context, token string) (*model.Registration, error) {
	s.log.Debug("Get", zap.String("token", fmt.Sprint(token)))
	result := []model.Registration{}

	for _, s := range s.registrationList {
		if s.Token == token {
			result = append(result, s)
			break
		}
	}

	if len(result) == 0 {
		return nil, ErrRegistrationNotFound
	}

	return &result[0], nil
}

func (s *RegistrationStore) Update(ctx context.Context, newRegistration model.Registration, token string) (*model.Registration, error) {
	s.log.Debug("Update", zap.String("token", fmt.Sprint(token)))

	found := false
	for i, n := range s.registrationList {
		if n.Token == token {
			s.registrationList[i].AppPublicKey = newRegistration.AppPublicKey
			s.registrationList[i].CreatedAt = newRegistration.CreatedAt
			s.registrationList[i].ExpiresAt = newRegistration.ExpiresAt
			s.registrationList[i].Token = newRegistration.Token
			s.registrationList[i].Bsn = newRegistration.Bsn
			s.registrationList[i].ClientId = newRegistration.ClientId
			found = true
			break
		}
	}

	if !found {
		return nil, ErrRegistrationNotFound
	}

	return &newRegistration, nil
}

func (s *RegistrationStore) Delete(ctx context.Context, token string) error {
	s.log.Debug("Delete", zap.String("token", fmt.Sprint(token)))

	var foundIndex *int
	for i, n := range s.registrationList {
		if n.Token == token {
			foundIndex = &i
			break
		}
	}

	if foundIndex == nil {
		return ErrRegistrationNotFound
	}

	s.registrationList = append(s.registrationList[:*foundIndex], s.registrationList[*foundIndex+1:]...)

	return nil
}

func (s *RegistrationStore) GetHealthCheck() healthcheck.Result {
	return healthcheck.Result{
		Name:         "registration-store",
		Status:       healthcheck.StatusOK,
		ResponseTime: 0,
		HealthChecks: []healthcheck.Result{},
	}
}
